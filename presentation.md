---
title: Agile autrement #1 : Échapper au « piège des fonctionnalités »
author: Mauko Quiroga-Alvarado
date: 6 février 2024
title-slide-attributes:
    data-background-image: "static/logo.svg"
    data-background-size: "auto 10%"
    data-background-position: "95% 95%"
---

# Contexte

Comment axer les feuilles de route agiles sur l'impact ?

---

# Situation

- Les équipes agiles de produits tombent dans le « piège des fonctionnalités »
- Concentrent leurs feuilles de route sur le développement de fonctionnalités
- Sans les aligner explicitement sur les objectifs poursuivis

---

![Feature-driven roadmap](images/feature-based-roadmap.jpg)

---

# Complication

- Focalisation sur l'effort plutôt que sur les résultats mesurables
- Manque d'alignement avec les objectifs stratégiques de l'organisation
- Difficulté à répondre aux besoins changeants des agents et des usagers

---

# Problématique

Comment les équipes produit agiles pourraient-elles passer à une approche de 
feuilles de route axées sur l'impact ?

Pour assurer la flexibilité face à l'évolution des besoins changeants des
agents et des usagers ?

Mais tout en répondant aux besoins de pilotage des décideurs et aux demandes de
transparence des citoyennes et des citoyens ?

---

# Solution : les feuilles de route axées sur l'impact

- Se concentrent sur l'atteinte des objectifs de l'organisation et du produit
- Plutôt que sur la simple livraison de fonctionnalités
- Reposent sur une vision claire des résultats attendus

---

![Outcome-driven roadmap](images/outcome-based-roadmap.png)

---

# Comment en construire une ?

---

# Étape 1 : Établir et prioriser des objectifs et des résultats clés (OKR) 

![Étape 1](images/e1.png)

---

# Étape 2 : Établir les initiatives (epic) prioritaires pour chaque OKR

![Étape 2](images/e2.png)

---

# Étape 3 : Établir les activités (user stories) pour chaque initiative (epic)    

![Étape 3](images/e3.png)

---

# Étape 4 : Ajuster la feuille de route en fonction des résultats obtenus

![Étape 4](images/e31.png)

---

# Étape 5 : Ajuster en fonction des demandes des utilisatrices et utilisateurs

![Étape 4](images/e32.png)

---

# Étape 5 : Se rendre redevable des résultats devant un COMEX

- Expliquer où vous en êtes en terme d’impact
- Donner envie de continuer en partageant vos perspectives d’impact et la stratégie pour y arriver
- Présenter les besoins associés à l’ambition pour arbitrage (ressources, appui politique, etc.)

(Source: [Préparer un comité d'investissement](https://doc.incubateur.net/communaute/gerer-sa-startup-detat-ou-de-territoires-au-quotidien/gestion-administrative/preparer-un-comite-dinvestissement))